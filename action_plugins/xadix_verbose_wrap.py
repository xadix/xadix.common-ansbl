from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible.errors import AnsibleAction, AnsibleActionFail
from ansible.plugins.action import ActionBase

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

class ActionModule(ActionBase):
    def run(self, tmp=None, task_vars=None):

        self._supports_check_mode = True
        self._supports_async = False

        if task_vars is None:
            task_vars = dict()

        results = super(ActionModule, self).run(tmp, task_vars)
        #display.vv("self %s" % self)
        #display.vv("self._task (%s)%s" % ( type(self._task), self._task ))
        #display.vv("self._task.name (%s)%s" % ( type(self._task.name), self._task.name ))
        #display.vv("self._task.args (%s)%s" % ( type(self._task.args), self._task.args ))
        module_name_candidates = list(set(self._task.args) - set(["args"]))
        if len( module_name_candidates ) > 1: raise AnsibleActionFail("Could not determine module name - more than one candidate %s" % ( module_name_candidates ))
        module_name = module_name_candidates[0]
        #display.vv("module_name (%s)%s" % ( type(module_name), module_name ))
        module_args = self._task.args[module_name]
        display.vv("module_args (%s)%s" % ( type(module_args), module_args ))
        module_args_fixed = None
        if isinstance(module_args, dict):
            module_args_fixed = module_args
        else:
            module_args_fixed = self._task.args.get("args", {})
            module_args_fixed["_raw_params"] = module_args
        #display.vv("module_args_fixed (%s)%s" % ( type(module_args_fixed), module_args_fixed ))
        display.display("%s: %s" % ( module_name, module_args_fixed ))
        results.update(self._execute_module(module_name=module_name, module_args=module_args_fixed, task_vars=task_vars))
        return results
